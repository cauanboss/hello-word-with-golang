
FROM golang:1.19-alpine as builder
WORKDIR /app
COPY . .
RUN go build -ldflags "-s -w" -o server .

FROM scratch
WORKDIR /app
COPY --from=builder /app/server server
CMD [ "./server" ]

